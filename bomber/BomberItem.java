package bomber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class BomberItem extends Item {
	
	private HashMap<EntityPlayer,ArrayList<Entity>> bomberMapServer;
	private HashMap<EntityPlayer,ArrayList<Entity>> bomberMapClient;
	
	public BomberItem(int id) {
		super(id);
		try {
			// Constructor Configuration
			setCreativeTab(CreativeTabs.tabMisc);
			setUnlocalizedName("bomberItem");
			this.bomberMapServer = new HashMap();
			this.bomberMapClient = new HashMap();
		} catch (ExceptionInInitializerError e) {
			System.out.println(e.getCause().getMessage());
			e.getCause().printStackTrace();
			throw e;
		}
	}

    /**
     * Called when the player Left Clicks (attacks) an entity.
     * Processed before damage is done, if return value is true further processing is canceled
     * and the entity is not attacked.
     *
     * @param stack The Item being used
     * @param player The player that is attacking
     * @param entity The entity being attacked
     * @return True to cancel the rest of the interaction.
     */
	@Override
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity)
    {
        System.out.println(Bomber.ModId + ": onLeftClickEntity - entity=<" + entity.toString() + ">");
    	getBombers(player).add(entity);
        return false;
    }
    
    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
	@Override
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
    	par2World.playSoundAtEntity(par3EntityPlayer, Bomber.ModId + ":bomberswitch", 0.5F, 0.4F);
    	for (Entity e: getBombers(par3EntityPlayer)) {
    		par2World.createExplosion(e, e.posX, e.posY, e.posZ, 6.0F, true);
    		e.setDead();
    	}
    	getBombers(par3EntityPlayer).clear();
        return par1ItemStack;
    }

    @SideOnly(Side.CLIENT)
	@Override
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.itemIcon = par1IconRegister.registerIcon(Bomber.ModId + ":bomberswitch");
        //System.out.println(Bomber.ModId + ": registerIcons - bomberswitch");
    }
    
    /**
     * Called when a player drops the item into the world,
     * returning false from this will prevent the item from
     * being removed from the players inventory and spawning
     * in the world
     *
     * @param player The player that dropped the item
     * @param item The item stack, before the item is removed.
     */
    public boolean onDroppedByPlayer(ItemStack item, EntityPlayer player)
    {
    	getBomberMap(player).remove(player);
        return true;
    }
    
    private HashMap<EntityPlayer, ArrayList<Entity>> getBomberMap(Entity player)
    {
    	if (player.worldObj.isRemote) {
        	return this.bomberMapServer;
    	}
        else {
        	return this.bomberMapClient;
        }
    }
    
    private ArrayList<Entity> getBombers(EntityPlayer player)
    {

        ArrayList<Entity> bombers = getBomberMap(player).get(player);
        if (bombers == null) {
        	bombers = new ArrayList();
        	getBomberMap(player).put(player, bombers);
        }
    	return bombers;
    }
}