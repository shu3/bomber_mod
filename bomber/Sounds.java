package bomber;

import net.minecraftforge.client.event.sound.SoundLoadEvent;
import net.minecraftforge.event.ForgeSubscribe;

public class Sounds {
	   @ForgeSubscribe
	   public void onSound(SoundLoadEvent event)
	   {
	       try
	       {
	    	   event.manager.addSound(Bomber.ModId + ":bomberswitch.ogg");
	       }
	       catch (Exception e)
	       {
	           System.err.println("Bomber Mod: Failed to register one or more sounds.");
	       }
	   }
}
